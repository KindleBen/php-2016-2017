#!/bin/bash

# update web site virtual host
cp /vagrant/provision/website-php-mod.conf /etc/apache2/sites-available/website.conf

# restart apache2
service apache2 restart

# import database
db_sql="/var/www/website/data/db.sql"

if [ -f $db_sql ]; then
	echo "db.sql file found"
	mysql --user=website --password=website website < /var/www/website/data/db.sql
fi

